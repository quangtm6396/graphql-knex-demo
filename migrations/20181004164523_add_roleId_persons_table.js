
exports.up = function(knex, Promise) {
    return knex.schema.table('persons', function(add) {
        add.integer('role_id').unsigned();
        add.foreign('role_id').references('roles.id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('persons', function(add) {
        add.dropColumn('role_id');
    })
};
