import { knex } from '../connectors';
import * as promise from 'bluebird';

class Person {
    getAllPersons() {
        knex.table('persons').innerJoin('roles','role_id','id')
        return knex('persons').select();
        return knex('roles').where('id', 'persons.id')
    }

    findPerson(id: number) {
        return knex('persons').where('id', id).first();
    }

    createPerson(input: object) {
        if (input) {
            return knex('persons').insert(input).then(function(result) {
                return knex('persons').where('id', result[0]).first();
            });
        }
        return false;
    }

    updatePerson(id: number, input: object) {
        if (id) {
            return knex('persons').where('id', id).update(input);
        }
        return false;
    }
}

export const PersonModel = new Person;